close all;
clear;
clc;

n = 1;
fontSize = 12;

%% TYPE SET
% EL = Mata Kiri
% ER = Mata Kanan
% ML = Mulut Kiri
% MR = Mulut Kanan
type        = 'EL'; % ubah nama tipe image fitur yg diggunakan 

uImageF     = 'EP04_04'; % ubah nama folder disini 
folder      = fullfile('E:/PENTING/Kerja/Face-BuRosi/Baru/POC/POCFIX/'); % ubah nama folder
folder_res  = fullfile(folder,sprintf('results/%s/%s',uImageF,type)); % folder untuk result
% folder untuk menyimpan image yg akan di proses 
folder_img  = fullfile(folder,sprintf('img/%s',uImageF)); 

sb_x = 8;
sb_y = 8;

T=2*sb_x;
range=1:T;
window=hann(T);
window=window.'*window;

mkdir(folder_res);
% perulangan untuk mengambil frame image
f_start = 31;
f_end   = 32;
for frame = f_start:f_end
    nameImage0 = sprintf('%s/reg_img%d.jpg',folder_img,frame);
    nameImage1 = sprintf('%s/reg_img%d.jpg',folder_img,frame+1);

    img0 = imread(nameImage0);
    img1 = imread(nameImage1);

    %% matrix dari setiap sub image yg sudah di pecah
    block_cur4 = createblock(img0,2,2 , 'fitur');
    block_ref4 = createblock(img1,2,2 , 'fitur');

    %% pemanggilan variable return dari fungsi createBlock()
    % block4(:,:,1) = Mata Kiri
    % block4(:,:,2) = Mata Kanan
    % block4(:,:,3) = Mulut Kiri
    % block4(:,:,4) = Mulut Kanan
    
    if strcmp(type,'EL')
        type_fitur = 1;
    elseif strcmp(type,'ER')
        type_fitur = 2;
    elseif strcmp(type, 'ML')
        type_fitur = 3;
    elseif strcmp(type,'MR')
        type_fitur = 4;
    end
    
    imgBlockCur = block_cur4(:,:,type_fitur);
    imgBlockRef = block_ref4(:,:,type_fitur);
    
    figure(1), subplot(3,1,1),imshow(imgBlockCur), title('Frame Sekarang');
    figure(1), subplot(3,1,2),imshow(imgBlockRef), title('Frame Selanjutnya');

    imgBlockCur = double(imgBlockCur);
    imgBlockRef = double(imgBlockRef);
    
    [frameY,frameX]=size(imgBlockCur);

    mv_y = zeros(fix(frameY/sb_y),fix(frameX/sb_x));
    mv_x = zeros(fix(frameY/sb_y),fix(frameX/sb_x));

    lRows = frameY/sb_y-1;
    lCols = frameX/sb_x-1;
    
    num = 0;
    for i=2:lRows,
        for j=2:lCols,
            batasy_1    = (i-1)*sb_y+1;
            batasy_2    = i*sb_y;
            batasx_1    = (j-1)*sb_x+1;
            batasx_2    = j*sb_x;

            blockReff   = imgBlockRef(batasy_1-(sb_y/2):batasy_2+(sb_y/2),batasx_1-(sb_x/2):batasx_2+(sb_x/2));
            blockCurr   = imgBlockCur(batasy_1-(sb_y/2):batasy_2+(sb_y/2),batasx_1-(sb_x/2):batasx_2+(sb_x/2)); 

            fft_ref     = fft2(blockReff.*window,sb_y*2,sb_x*2);
            fft_curr    = fft2(blockCurr.*window,sb_y*2,sb_x*2);

            R1          = fft_curr.*conj(fft_ref);
            R2          = abs(R1);
            R2(R2==0)   = 1e-31;
            R           = R1./R2;
            r           = fftshift(abs(ifft2(R)));
            POC(:,:,i)  = r;   
            
            save([folder_res '\\POC_block_ke' num2str(num) '_' num2str(i) '.mat'],'r');
            
            max_r       = r==max(max(r));
            [temp_y, temp_x]=find(max_r);
            mv_y(i,j)   = temp_y(1)-sb_y-1;
            mv_x(i,j)   = temp_x(1)-sb_x-1;
            
            num = num+1;
        end
    end
    
     figure(1), subplot(3,1,3) ,quiver(mv_x,mv_y), title('QUIVER');
end

