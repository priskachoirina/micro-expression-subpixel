%% Fungsi untuk membuat blok
% img = image yg akan dibagi
% blockR = matrix pembagi row atau tinggi
% blockC = matrix pembagi column atau lebar 
% return image 3D menampung semua matrix image hasil pembagian block 
% image3D(:,:,1) = Mata Kiri
% image3D(:,:,2) = Mata Kanan
% image3D(:,:,3) = Mulut Kiri
% image3D(:,:,4) = Mulut Kanan

function [ oneBlock] = createblock( img, blockR, blockC, type )
    
    % rows = height , columns= width 
    [rows, columns, oNum] = size(img);
   
    %% Pembagian 4 block image
    if(strcmp(type, 'fitur'))
        blockSizeH = floor(rows / blockR); %r 
        blockSizeW = floor(columns /blockC);% c 
    else
        blockSizeH = blockR; %r 
        blockSizeW = blockC;% c 
    end
        
    
    %% Mengetahui ukuran dari setiap blok
    blockRows = fix(rows / blockSizeH);
    blockCols = fix(columns / blockSizeW); 
    %% alokasi untuk image 3d
    %image3d = zeros(blockRows, blockCols, 3);
    
    %% Perulangan untuk mendapatkan 4 blok image
    sliceNumber = 1;
    for row = 1 : blockSizeH : rows
        for col = 1 : blockSizeW : columns
            row1 = row;
            row2 = row1 + blockSizeH - 1;
            col1 = col;
            col2 = col1 + blockSizeW - 1;
            
            if(row1 ~= rows && col1 ~= columns && row2 <= rows && col2 <= columns)
                % Ekstraksi image kedalam subblock
                oneBlock(:, :, sliceNumber) = img(row1:row2, col1:col2,oNum);
                sliceNumber = sliceNumber + 1;
            end 
        end
    end
end

